# -*- encoding: utf-8 -*-

Gem::Specification.new do |s|
  s.name = "em-twitter"
  s.version = "0.3.3"

  s.required_rubygems_version = Gem::Requirement.new(">= 0") if s.respond_to? :required_rubygems_version=
  s.authors = ["Steve Agalloco"]
  s.date = "2014-04-25"
  s.description = "Twitter Streaming API client for EventMachine"
  s.email = ["steve.agalloco@gmail.com"]
  s.files = [".yardopts", "CONTRIBUTING.md", "LICENSE.md", "README.md", "Rakefile", "em-twitter.gemspec", "lib/em-twitter/response.rb", "lib/em-twitter/request.rb", "lib/em-twitter/proxy.rb", "lib/em-twitter/decoders/gzip_decoder.rb", "lib/em-twitter/decoders/base_decoder.rb", "lib/em-twitter/reconnectors/network_failure.rb", "lib/em-twitter/reconnectors/application_failure.rb", "lib/em-twitter/version.rb", "lib/em-twitter/client.rb", "lib/em-twitter/connection.rb", "lib/em-twitter.rb", "lib/em_twitter.rb", "spec/em-twitter", "spec/em-twitter/connection_reconnect_spec.rb", "spec/em-twitter/proxy_spec.rb", "spec/em-twitter/request_spec.rb", "spec/em-twitter/decoders", "spec/em-twitter/decoders/gzip_decoder_spec.rb", "spec/em-twitter/decoders/base_decoder_spec.rb", "spec/em-twitter/reconnectors", "spec/em-twitter/reconnectors/network_failure_spec.rb", "spec/em-twitter/reconnectors/application_failure_spec.rb", "spec/em-twitter/client_spec.rb", "spec/em-twitter/response_spec.rb", "spec/em-twitter/connection_spec.rb", "spec/em-twitter/connection_error_handling_spec.rb", "spec/em_twitter_spec.rb", "spec/spec_helper.rb"]
  s.homepage = "https://github.com/spagalloco/em-twitter"
  s.licenses = ["MIT"]
  s.require_paths = ["lib"]
  s.rubygems_version = "1.8.25"
  s.summary = "Twitter Streaming API client for EventMachine"

  if s.respond_to? :specification_version then
    s.specification_version = 3

    if Gem::Version.new(Gem::VERSION) >= Gem::Version.new('1.2.0') then
      s.add_runtime_dependency(%q<eventmachine>, ["~> 1.0"])
      s.add_runtime_dependency(%q<http_parser.rb>, ["~> 0.6"])
      s.add_runtime_dependency(%q<simple_oauth>, ["~> 0.2"])
      s.add_runtime_dependency(%q<buftok>, ["~> 0.2"])
      s.add_development_dependency(%q<bundler>, ["~> 1.0"])
    else
      s.add_dependency(%q<eventmachine>, ["~> 1.0"])
      s.add_dependency(%q<http_parser.rb>, ["~> 0.6"])
      s.add_dependency(%q<simple_oauth>, ["~> 0.2"])
      s.add_dependency(%q<buftok>, ["~> 0.2"])
      s.add_dependency(%q<bundler>, ["~> 1.0"])
    end
  else
    s.add_dependency(%q<eventmachine>, ["~> 1.0"])
    s.add_dependency(%q<http_parser.rb>, ["~> 0.6"])
    s.add_dependency(%q<simple_oauth>, ["~> 0.2"])
    s.add_dependency(%q<buftok>, ["~> 0.2"])
    s.add_dependency(%q<bundler>, ["~> 1.0"])
  end
end
