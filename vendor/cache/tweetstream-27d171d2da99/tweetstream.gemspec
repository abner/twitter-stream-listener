# -*- encoding: utf-8 -*-

Gem::Specification.new do |s|
  s.name = "tweetstream"
  s.version = "2.6.1"

  s.required_rubygems_version = Gem::Requirement.new(">= 0") if s.respond_to? :required_rubygems_version=
  s.authors = ["Michael Bleigh", "Steve Agalloco"]
  s.date = "2014-04-25"
  s.description = "TweetStream is a simple wrapper for consuming the Twitter Streaming API."
  s.email = ["michael@intridea.com", "steve.agalloco@gmail.com"]
  s.files = [".yardopts", "CHANGELOG.md", "CONTRIBUTING.md", "LICENSE.md", "README.md", "Rakefile", "tweetstream.gemspec", "lib/tweetstream/site_stream_client.rb", "lib/tweetstream/arguments.rb", "lib/tweetstream/daemon.rb", "lib/tweetstream/version.rb", "lib/tweetstream/configuration.rb", "lib/tweetstream/client.rb", "lib/tweetstream.rb", "spec/fixtures", "spec/fixtures/info.json", "spec/fixtures/favorite.json", "spec/fixtures/ids.json", "spec/fixtures/scrub_geo.json", "spec/fixtures/statuses.json", "spec/fixtures/delete.json", "spec/fixtures/status_withheld.json", "spec/fixtures/stall_warning.json", "spec/fixtures/limit.json", "spec/fixtures/user_withheld.json", "spec/fixtures/direct_messages.json", "spec/tweetstream", "spec/tweetstream/site_stream_client_spec.rb", "spec/tweetstream/client_userstream_spec.rb", "spec/tweetstream/client_spec.rb", "spec/tweetstream/client_site_stream_spec.rb", "spec/tweetstream/daemon_spec.rb", "spec/tweetstream/client_authentication_spec.rb", "spec/tweetstream_spec.rb", "spec/helper.rb"]
  s.homepage = "https://github.com/tweetstream/tweetstream"
  s.licenses = ["MIT"]
  s.require_paths = ["lib"]
  s.rubygems_version = "1.8.25"
  s.summary = "TweetStream is a simple wrapper for consuming the Twitter Streaming API."
  s.test_files = ["spec/fixtures", "spec/fixtures/info.json", "spec/fixtures/favorite.json", "spec/fixtures/ids.json", "spec/fixtures/scrub_geo.json", "spec/fixtures/statuses.json", "spec/fixtures/delete.json", "spec/fixtures/status_withheld.json", "spec/fixtures/stall_warning.json", "spec/fixtures/limit.json", "spec/fixtures/user_withheld.json", "spec/fixtures/direct_messages.json", "spec/tweetstream", "spec/tweetstream/site_stream_client_spec.rb", "spec/tweetstream/client_userstream_spec.rb", "spec/tweetstream/client_spec.rb", "spec/tweetstream/client_site_stream_spec.rb", "spec/tweetstream/daemon_spec.rb", "spec/tweetstream/client_authentication_spec.rb", "spec/tweetstream_spec.rb", "spec/helper.rb"]

  if s.respond_to? :specification_version then
    s.specification_version = 3

    if Gem::Version.new(Gem::VERSION) >= Gem::Version.new('1.2.0') then
      s.add_runtime_dependency(%q<daemons>, ["~> 1.1"])
      s.add_runtime_dependency(%q<em-http-request>, [">= 1.1.1"])
      s.add_runtime_dependency(%q<em-twitter>, ["~> 0.3"])
      s.add_runtime_dependency(%q<twitter>, ["~> 5.5"])
      s.add_runtime_dependency(%q<multi_json>, ["~> 1.3"])
      s.add_development_dependency(%q<bundler>, ["~> 1.0"])
    else
      s.add_dependency(%q<daemons>, ["~> 1.1"])
      s.add_dependency(%q<em-http-request>, [">= 1.1.1"])
      s.add_dependency(%q<em-twitter>, ["~> 0.3"])
      s.add_dependency(%q<twitter>, ["~> 5.5"])
      s.add_dependency(%q<multi_json>, ["~> 1.3"])
      s.add_dependency(%q<bundler>, ["~> 1.0"])
    end
  else
    s.add_dependency(%q<daemons>, ["~> 1.1"])
    s.add_dependency(%q<em-http-request>, [">= 1.1.1"])
    s.add_dependency(%q<em-twitter>, ["~> 0.3"])
    s.add_dependency(%q<twitter>, ["~> 5.5"])
    s.add_dependency(%q<multi_json>, ["~> 1.3"])
    s.add_dependency(%q<bundler>, ["~> 1.0"])
  end
end
