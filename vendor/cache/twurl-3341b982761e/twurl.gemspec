# -*- encoding: utf-8 -*-

Gem::Specification.new do |s|
  s.name = "twurl"
  s.version = "0.9.2"

  s.required_rubygems_version = Gem::Requirement.new(">= 1.3.5") if s.respond_to? :required_rubygems_version=
  s.authors = ["Marcel Molina", "Erik Michaels-Ober"]
  s.date = "2014-04-25"
  s.description = "Curl for the Twitter API"
  s.email = ["marcel@twitter.com"]
  s.executables = ["twurl"]
  s.extra_rdoc_files = ["COPYING", "INSTALL", "README"]
  s.files = [".gemtest", ".gitignore", ".travis.yml", "COPYING", "Gemfile", "INSTALL", "README", "Rakefile", "bin/twurl", "lib/twurl.rb", "lib/twurl/abstract_command_controller.rb", "lib/twurl/account_information_controller.rb", "lib/twurl/aliases_controller.rb", "lib/twurl/authorization_controller.rb", "lib/twurl/cli.rb", "lib/twurl/configuration_controller.rb", "lib/twurl/oauth_client.rb", "lib/twurl/rcfile.rb", "lib/twurl/request_controller.rb", "lib/twurl/version.rb", "test/account_information_controller_test.rb", "test/alias_controller_test.rb", "test/authorization_controller_test.rb", "test/cli_options_test.rb", "test/cli_test.rb", "test/configuration_controller_test.rb", "test/oauth_client_test.rb", "test/rcfile_test.rb", "test/request_controller_test.rb", "test/test_helper.rb", "twurl.gemspec"]
  s.homepage = "http://github.com/twitter/twurl"
  s.licenses = ["MIT"]
  s.rdoc_options = ["--title", "twurl -- OAuth-enabled curl for the Twitter API", "--main", "README", "--line-numbers", "--inline-source"]
  s.require_paths = ["lib"]
  s.rubyforge_project = "twurl"
  s.rubygems_version = "1.8.25"
  s.summary = "Curl for the Twitter API"

  if s.respond_to? :specification_version then
    s.specification_version = 3

    if Gem::Version.new(Gem::VERSION) >= Gem::Version.new('1.2.0') then
      s.add_runtime_dependency(%q<oauth>, ["~> 0.4"])
      s.add_development_dependency(%q<bundler>, ["~> 1.0"])
    else
      s.add_dependency(%q<oauth>, ["~> 0.4"])
      s.add_dependency(%q<bundler>, ["~> 1.0"])
    end
  else
    s.add_dependency(%q<oauth>, ["~> 0.4"])
    s.add_dependency(%q<bundler>, ["~> 1.0"])
  end
end
