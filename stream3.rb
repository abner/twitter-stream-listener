require 'oauth'
require 'twurl'
require 'ostruct'
require 'logger'
require 'http_logger'

Net::HTTP.logger = Logger.new(STDOUT) # defaults to Rails.logger if Rails is defined
Net::HTTP.colorize = true # Default: true

#Twurl.options = Twurl::Options.new

def twitter_stream(oauth_data, tracking_data, output=nil, proxy = nil)
  Twurl.options.consumer_key = oauth_data["TWITTER_CONSUMER_KEY"]
  Twurl.options.consumer_secret = oauth_data["TWITTER_CONSUMER_SECRET"]
  Twurl.options.access_token = oauth_data["TWITTER_ACCESS_TOKEN"]
  Twurl.options.token_secret = oauth_data["TWITTER_TOKEN_SECRET"]
  Twurl.options.username = "abneroliveira"
  Twurl.options.data = tracking_data
  Twurl.options.host = "stream.twitter.com"
  Twurl.options.path = "/1.1/statuses/filter.json"
  Twurl.options.request_method = :post
  Twurl.options.command = 'request'
  #Twurl.options.trace = true

  Twurl.options.output = output if output

  Twurl.options.proxy = proxy

  client = Twurl::OAuthClient.load_from_options(Twurl.options)

  controller = Twurl::RequestController

  controller.dispatch(client, Twurl.options)
end

require 'my_oauth_data.rb'



oauth_data =  {
  'TWITTER_CONSUMER_KEY' => ENV["TWITTER_CONSUMER_KEY"],
  'TWITTER_CONSUMER_SECRET' => ENV["TWITTER_CONSUMER_SECRET"],
  'TWITTER_ACCESS_TOKEN' => ENV["TWITTER_ACCESS_TOKEN"],
  'TWITTER_TOKEN_SECRET' => ENV["TWITTER_TOKEN_SECRET"],
}

file_output = File.open('output.txt', 'w+')

proxy = "" #"http://161.148.1.167:312"

begin
  twitter_stream(oauth_data, { 'track' => 'nba'}, file_output)
rescue
  file_output.close
end